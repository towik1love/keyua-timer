import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
`;
export const P = styled.p`
  font-size: 24px;
  font-weight: bold;
  margin: 0 20px;
`;
export const StyledInput = styled.input`
  padding: 10px;
  font-size: 24px;
  outline: none;
  box-sizing: border-box;
  width: 200px;
`;
export const Button = styled.button`
  cursor: pointer;
  margin: 0 20px;
  text-transform: uppercase;
  padding: 12px;
  border: none;
  outline: none;
  font-size: 24px;
  color: #fff;
  background: ${({ isActive }) =>
    isActive ? "rgb(233,67,54)" : "rgb(82,182,156)"};
  transition: 0.2s;
  &:hover {
    opacity: 0.9;
  }
`;
export const ErrorMsg = styled.p`
  position: absolute;
  color: rgb(233, 67, 54);
  left:150px;
`;
