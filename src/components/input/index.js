import React from "react";
import PropTypes from "prop-types";
import { Wrapper, P, StyledInput, Button, ErrorMsg } from "./elements";

const Input = ({ onChange, onStart, isActive, onReset, min, err }) => (
  <div style={{ position: "relative" }}>
    <Wrapper>
      <P>Countdown:</P>
      <StyledInput
        type="text"
        placeholder="(Min)"
        onChange={onChange}
        value={isActive ? "" : min}
      />

      <Button isActive={isActive} onClick={isActive ? onReset : onStart}>
        {isActive ? "reset" : "start"}
      </Button>
    </Wrapper>
    {err !== "" && <ErrorMsg>{err}</ErrorMsg>}
  </div>
);

Input.propTypes = {
  onChange: PropTypes.func,
  onStart: PropTypes.func,
  onReset: PropTypes.func,
  isActive: PropTypes.bool,
  min: PropTypes.string,
  err: PropTypes.string
};

Input.defaultProps = {
  onChange: () => {},
  onStart: () => {},
  onReset: () => {},
  isActive: false,
  min: "",
  err: ""
};

export default Input;
