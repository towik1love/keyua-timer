import React from "react";
import PropTypes from "prop-types";
import { H1, P, Wrapper } from "./elements";
import Pause from "../buttons";

const addLeadingZeros = value => {
  value = String(value);
  while (value.length < 2) {
    value = "0" + value;
  }
  return value;
};

const Clock = ({ secLeft, flagTime, paused, onPause }) => (
  <Wrapper>
    {flagTime >= secLeft && secLeft !== 0 && flagTime !== 0 && (
      <P>More than halfway there!</P>
    )}
    {secLeft === 0 && flagTime === 1 && <P> Time's up!</P>}
    <H1 secLeft={secLeft} flagTime={flagTime}>
      {addLeadingZeros(Math.floor(secLeft / 60))}:
      {addLeadingZeros(secLeft - Math.floor(secLeft / 60) * 60)}
    </H1>
    <Pause secLeft={secLeft} onPause={onPause} paused={paused} />
  </Wrapper>
);

Clock.propTypes = {
  onPause: PropTypes.func,
  paused: PropTypes.bool,
  secLeft: PropTypes.number,
  flagTime: PropTypes.number
};

Clock.defaultProps = {
  onPause: () => {},
  paused: false,
  secLeft: null,
  flagTime: null
};

export default Clock;
