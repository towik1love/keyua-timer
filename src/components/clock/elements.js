import styled from "styled-components";

export const H1 = styled.h1`
  margin: 20px 0;
  font-size: 160px;
  color: ${({ secLeft, flagTime }) =>
    secLeft <= 20 && flagTime > 0 ? "rgb(233,67,54)" : "#000"};
  animation: ${({ secLeft, flagTime }) =>
    secLeft <= 10 && flagTime > 1 ? "blink 0.8s linear infinite" : ""};
  @keyframes blink {
    50% {
      color: rgb(233, 67, 54);
    }
    51% {
      color: rgb(233, 67, 54, 0);
    }
    100% {
      color: rgb(233, 67, 54, 0);
    }
  }
`;
export const P = styled.p`
  text-align: center;
  font-size: 24px;
  font-style: italic;
  width: 270px;
  position: absolute;
  top: 20px;
  left: 80px;
  margin: 0;
`;

export const Wrapper = styled.div`
  position: relative;
`;
