import React from "react";
import PropTypes from "prop-types";
import { Button } from "./elements";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Pause = ({ onPause, paused, secLeft }) => (
  <div>
    {secLeft !== 0 && (
      <Button onClick={onPause}>
        {paused ? (
          <FontAwesomeIcon icon="play-circle" />
        ) : (
          <FontAwesomeIcon icon="pause-circle" />
        )}
      </Button>
    )}
  </div>
);

Pause.propTypes = {
  onPause: PropTypes.func,
  paused: PropTypes.bool,
  secLeft: PropTypes.number
};

Pause.defaultProps = {
  onPause: () => {},
  paused: false,
  secLeft: null
};

export default Pause;
