import styled from "styled-components";

export const Button = styled.button`
  background: transparent;
  border: none;
  outline: none;
  position: absolute;
  font-size: 100px;
  right: -165px;
  top: 85px;
  color: green;
  cursor: pointer;
`;
