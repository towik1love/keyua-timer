import React from "react";
import PropTypes from "prop-types";
import { StyledInput } from "./elements";
import { Wrapper } from "./elements";

const Speedlist = [
  {
    val: "1X",
    speed: 1000
  },
  {
    val: "1.5X",
    speed: 750
  },
  {
    val: "2X",
    speed: 500
  }
];

const Speed = ({ changeSpeed, paused, secLeft, speed }) => (
  <>
    {secLeft !== 0 && !paused && (
      <Wrapper>
        {Speedlist.map((item,index) => (
          <StyledInput
            key={index}
            speed={item.speed}
            curSpeed={speed}
            type="submit"
            value={item.val}
            onClick={() => changeSpeed(item.speed)}
          />
        ))}
      </Wrapper>
    )}
  </>
);

Speed.propTypes = {
  changeSpeed: PropTypes.func,
  paused: PropTypes.bool,
  secLeft: PropTypes.number,
  speed: PropTypes.number,
};

Speed.defaultProps = {
  changeSpeed: () => {},
  paused: false,
  secLeft: null,
  speed: null
};

export default Speed;
