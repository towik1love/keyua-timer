import styled from "styled-components";

export const StyledInput = styled.input`
  padding: 10px;
  font-size: 24px;
  outline: none;
  box-sizing: border-box;
  width: 100px;
  background: ${({ curSpeed, speed }) =>
    curSpeed === speed ? "grey" : "transparent"};
  color: ${({ curSpeed, speed }) => (curSpeed === speed ? "#fff" : "#000")};
  border: 1px solid #000;
  margin: 0 10px;
  cursor: pointer;
`;
export const Wrapper = styled.div`
  position: absolute;
  bottom: 150px;
`;
