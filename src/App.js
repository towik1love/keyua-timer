import React, { Component } from "react";
import Input from "./components/input";
import Clock from "./components/clock";
import Speed from "./components/speed";
import "./App.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPauseCircle, faPlayCircle } from "@fortawesome/free-solid-svg-icons";

library.add(faPauseCircle, faPlayCircle);

class App extends Component {
  state = {
    min: "",
    secLeft: 0,
    speed: 1000,
    flagTime: 0,
    isActive: false,
    paused: false,
    err: ""
  };

  handleChange = ({ target: { value } }) => {
    if (!isNaN(value) && value.length <= 2) {
      this.setState({
        min: value
      });
    }
  };

  validTime = val => {
    if (parseInt(val) * 60 <= 3540 && parseInt(val) !== 0) {
      return true;
    }
  };

  onStart = () => {
    const { min, speed } = this.state;
    if (this.validTime(min)) {
      this.intervalHandle = setInterval(this.tick, speed);
      this.setState({
        secLeft: parseInt(min) * 60,
        flagTime: (parseInt(min) * 60) / 2,
        isActive: true,
        min: "",
        err: ""
      });
    } else {
      this.setState({
        err: "Permissible value <=59 min and not 0"
      });
    }
  };

  onReset = () => {
    clearInterval(this.intervalHandle);
    this.setState({
      min: "",
      secLeft: 0,
      speed: 1000,
      flagTime: 0,
      isActive: false,
      paused: false
    });
  };

  tick = () => {
    const { secLeft, isActive } = this.state;
    if (isActive) {
      this.setState({
        secLeft: secLeft - 1
      });
      if (secLeft === 1) {
        clearInterval(this.intervalHandle);
        this.setState({
          secLeft: 0,
          isActive: false,
          flagTime: 1
        });
      }
    }
  };

  changeSpeed = val => {
    clearInterval(this.intervalHandle);
    this.setState(
      {
        speed: val
      },
      () => {
        const { speed } = this.state;
        this.intervalHandle = setInterval(this.tick, speed);
      }
    );
  };
  onPause = () => {
    const { paused, speed } = this.state;
    if (paused) {
      this.intervalHandle = setInterval(this.tick, speed);
      this.setState({ paused: !paused });
    } else {
      this.setState({ paused: !paused });
      clearInterval(this.intervalHandle);
    }
  };

  render() {
    const { min, isActive, secLeft, paused, flagTime, speed, err } = this.state;
    return (
      <div className="wrapper">
        <Input
          err={err}
          min={min}
          isActive={isActive}
          onChange={this.handleChange}
          onStart={this.onStart}
          onReset={this.onReset}
        />
        <Clock
          flagTime={flagTime}
          secLeft={secLeft}
          isActive={isActive}
          onPause={this.onPause}
          paused={paused}
        />
        <Speed
          speed={speed}
          secLeft={secLeft}
          paused={paused}
          changeSpeed={this.changeSpeed}
        />
      </div>
    );
  }
}

export default App;
